/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
    root: true,
    extends: [
        'plugin:vue/vue3-essential',
        'eslint:recommended',
        '@vue/eslint-config-prettier'
    ],
    rules: {
        // allow paren-less arrow functions
        'arrow-parens': 0,
        // allow async-await
        'generator-star-spacing': 0,
        // allow variables like >> user_id
        camelcase: 0,
        'prettier/prettier': [
            'error',
            {
                endOfLine: 'auto'
            }
        ],
        'no-useless-catch': 0,
        'no-trailing-spaces': ['error', { skipBlankLines: true }],
        'vue/multi-word-component-names': 0,
        'vue/no-reserved-component-names': 0,
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
    },
    parserOptions: {
        ecmaVersion: 'latest'
    }
}
