export default {
    TOKEN: 'TOKEN',
    USERNAME: 'USERNAME',
    SESSION: 'SESSION',
    LANGUAGE: 'LANGUAGE'
}
