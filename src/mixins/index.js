/**
 * import and init global mixins
 */

import currentUser from '../mixins/currentUser'
import jumpTo from '../mixins/jumpTo'
import formatDateTime from '../mixins/formatDateTime'

export default {
   currentUser,
   jumpTo,
   formatDateTime
}
