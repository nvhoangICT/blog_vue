import { createApp } from 'vue'

import AppLayout from './layout/index.vue'
import router from './router'
import store from './store'

import mixins from './mixins'
import './assets/fonts/bebasneue.css'

export const app = createApp(AppLayout)
app.use(router)
app.use(store)
app.mixin(mixins)
app.mount('#app')
