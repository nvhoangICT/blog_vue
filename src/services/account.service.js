import { post } from '@/services/request'
import { wsCode } from './wsCode'

export const getSim = async () => {
    try {
        const result = await post(wsCode.wsSearchIsdn, {
            phoneNumber: '',
            simType: '023',
            page: 1,
            itemsPerPage: 6
        })
        console.log('result', result)
    } catch (error) {
        console.log('errror', error)
    }
}

export const login = async () => {
    try {
        const result = await post(
            '',
            {
                password: '123456aA@',
                prefix: '856',
                accountType: '0',
                clientOS: '1',
                language: 'en',
                appCode: 'MyVTG',
                username: '2095144190',
                isEncrypt: '0'
            },
            'ApigwGateway/CoreService/UserLogin'
        )
        console.log('result', result)
    } catch (error) {
        console.log('errror', error)
    }
}
