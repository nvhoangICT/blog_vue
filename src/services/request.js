import axios from 'axios'
import { keyStorage } from '@/config'
const client = axios.create({
    baseURL: import.meta.env.VITE_DOMAIN
})

client.interceptors.response.use(
    (response) => {
        return response.data
    },
    async (error) => {
        return Promise.reject(error)
    }
)

export const post = async (
    wsCode = '',
    wsRequest = {},
    url = import.meta.env.VITE_API_POST
) => {
    try {
        const body = {
            username: localStorage.getItem(keyStorage.USERNAME) || '',
            wsCode,
            wsRequest
        }
        const result = await client.post(url, wsCode ? body : wsRequest)
        if (result) {
            if (!wsCode) {
                if (result?.errorCode) {
                    throw result
                }

                return result
            }
            return result?.result
        }
    } catch (error) {
        throw error
    }
}

export default client
