/* eslint-disable no-undef */
import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'

export default ({ mode }) => {
   process.env = { ...process.env, ...loadEnv(mode, process.cwd()) }

   return defineConfig({
      plugins: [vue()],
      resolve: {
         alias: [
            {
               // this is required for the SCSS modules
               find: /^~(.*)$/,
               replacement: '$1'
            },
            {
               find: '@',
               replacement: resolve(__dirname, './src')
            }
         ],
         extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue', '.scss']
      },
      css: {
         preprocessorOptions: {
            scss: {
               additionalData: `@import "./src/assets/scss/style.scss";`
            }
         }
      },
      server: {
         port: process.env.VITE_PORT
      }
   })
}
